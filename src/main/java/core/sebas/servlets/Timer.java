package core.sebas.servlets;

import org.apache.log4j.Logger;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Timer extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Timer.class);

	@Override
	protected void doGet(HttpServletRequest request,final HttpServletResponse response) {
		try {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/html/timer.jsp");
			requestDispatcher.forward(request, response);
		}
		catch (ServletException se) {
			log.error(se.getMessage());
		}
		catch (IOException ie) {
			log.error(ie.getMessage());
		}
	}
	
}
